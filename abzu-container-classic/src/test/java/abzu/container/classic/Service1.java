package abzu.container.classic;

import abzu.container.api.AnnotatedBindings;
import abzu.container.api.TypedBindings;

public interface Service1 {
    Service0 getService0();

    AnnotatedBindings<Annotation1> getAnnotation1Bindings();

    TypedBindings<Service0> getService0Bindings();

    TypedBindings<String> getStringBindings();
}
