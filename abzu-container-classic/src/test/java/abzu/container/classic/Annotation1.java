package abzu.container.classic;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Annotation1 {
    Annotation1 INSTANCE = new Annotation1() {
        @Override
        public Class<? extends Annotation> annotationType() {
            return Annotation1.class;
        }
    };
}
