package abzu.container.classic;

import abzu.container.api.AbstractBinder;
import abzu.container.api.ContainerFactory;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ClassicContainerTest {

    @Test
    public void shouldBindContractAsImplementation() {
        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0Impl0.class);
                    }
                })
                .build()
                .create();
        assertTrue(container.has(Service0Impl0.class));
        final var serviceImpl0 = container.get(Service0Impl0.class);
        assertNotNull(serviceImpl0);
        final var oService0Impl0 = container.resolve(Service0Impl0.class);
        assertTrue(oService0Impl0.isPresent());
        final var oService0Impl1 = container.resolve(Service0Impl1.class);
        assertFalse(oService0Impl1.isPresent());
    }

    @Test
    public void shouldBindImplementation() {
        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0.class).to(Service0Impl0.class);
                    }
                })
                .build()
                .create();
        assertTrue(container.has(Service0.class));
        final var service0 = container.get(Service0.class);
        assertNotNull(service0);
    }

    @Test
    public void shouldBindValue() {
        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0.class).to(new Service0Impl0());
                    }
                })
                .build()
                .create();
        assertTrue(container.has(Service0.class));
        final var service0 = container.get(Service0.class);
        assertNotNull(service0);
    }

    @Test
    public void shouldBindValueFactoryType() {
        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0.class).toFactory(Service0FactoryImpl.class);
                    }
                })
                .build()
                .create();
        assertTrue(container.has(Service0.class));
        final var service0 = container.get(Service0.class);
        assertNotNull(service0);
    }

    @Test
    public void shouldBindValueFactory() {
        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0.class).toFactory(new Service0FactoryImpl());
                    }
                })
                .build()
                .create();
        assertTrue(container.has(Service0.class));
        final var service0 = container.get(Service0.class);
        assertNotNull(service0);
    }

    @Test
    public void shouldListValues() {
        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0.class).to(Service0Impl0.class).low();
                        bind(Service0.class).to(Service0Impl1.class);
                        bind(Service0.class).to(Service0Impl2.class).high();
                        bind(Service0.class).to(Service0Impl3.class);
                    }
                })
                .build()
                .create();
        assertTrue(container.has(Service0.class));
        final var service0 = container.list(Service0.class);
        assertNotNull(service0);
        assertEquals(4, service0.size());
        assertEquals(Service0Impl2.class, service0.get(0).getClass());
        assertEquals(Service0Impl1.class, service0.get(1).getClass());
        assertEquals(Service0Impl3.class, service0.get(2).getClass());
        assertEquals(Service0Impl0.class, service0.get(3).getClass());
    }

    @Test
    public void shouldListAnnotatedBindings() {
        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0.class).to(Service0Impl0.class).annotate(Annotation1.INSTANCE).low();
                        bind(Service0.class).to(Service0Impl1.class).annotate(Annotation1.INSTANCE);
                        bind(Service0.class).to(Service0Impl2.class).annotate(Annotation1.INSTANCE).high();
                        bind(Service0.class).to(Service0Impl3.class).annotate(Annotation1.INSTANCE);
                    }
                })
                .build()
                .create();

        final var annotation1Bindings = container.annotated(Annotation1.class);
        assertNotNull(annotation1Bindings);
        assertEquals(Annotation1.class, annotation1Bindings.getType());

        final var annotation1Values = annotation1Bindings.list();
        assertNotNull(annotation1Values);
        assertEquals(4, annotation1Values.size());
        assertEquals(Service0Impl2.class, annotation1Values.get(0).resolve().orElseThrow().getClass());
        assertEquals(Service0Impl1.class, annotation1Values.get(1).resolve().orElseThrow().getClass());
        assertEquals(Service0Impl3.class, annotation1Values.get(2).resolve().orElseThrow().getClass());
        assertEquals(Service0Impl0.class, annotation1Values.get(3).resolve().orElseThrow().getClass());

        assertNotNull(annotation1Values.get(0).getAnnotation(Annotation1.class));
        assertTrue(annotation1Values.get(0).isAnnotated(Annotation1.class));

        final var annotation2Bindings = container.annotated(Annotation2.class);
        assertNotNull(annotation2Bindings);
        assertEquals(Annotation2.class, annotation2Bindings.getType());

        final var annotation2Values = annotation2Bindings.list();
        assertNotNull(annotation2Values);
        assertEquals(4, annotation2Values.size());
        assertEquals(Service0Impl2.class, annotation2Values.get(0).resolve().orElseThrow().getClass());
        assertEquals(Service0Impl1.class, annotation2Values.get(1).resolve().orElseThrow().getClass());
        assertEquals(Service0Impl3.class, annotation2Values.get(2).resolve().orElseThrow().getClass());
        assertEquals(Service0Impl0.class, annotation2Values.get(3).resolve().orElseThrow().getClass());

        final var annotation3Bindings = container.annotated(Annotation3.class);
        assertNotNull(annotation3Bindings);
        assertEquals(Annotation3.class, annotation3Bindings.getType());
        assertTrue(annotation3Bindings.list().isEmpty());
    }

    @Test
    public void shouldDisposeFactory() {
        final var factory = mock(Service0Factory.class);
        final var service0 = mock(Service0.class);

        when(factory.get()).thenReturn(service0);

        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0.class).toFactory(factory);
                    }
                })
                .build()
                .create();

        container.get(Service0.class);
        verify(factory, times(1)).get();

        container.dispose();
        verify(factory, times(1)).dispose(service0);
    }

    @Test
    public void shouldInject() {
        final var container = ContainerFactory.builder()
                .binder(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(Service0.class).to(Service0Impl0.class).annotate(Annotation1.INSTANCE).low();
                        bind(Service0.class).to(Service0Impl1.class).annotate(Annotation1.INSTANCE);
                        bind(Service0.class).to(Service0Impl2.class).annotate(Annotation1.INSTANCE).high();
                        bind(Service0.class).to(Service0Impl3.class).annotate(Annotation1.INSTANCE);
                        bind(Service1.class).to(Service1Impl0.class);
                    }
                })
                .build()
                .create();

        final var service1 = container.get(Service1.class);
        assertNotNull(service1.getService0());
        assertNotNull(service1.getAnnotation1Bindings());
        assertEquals(4, service1.getAnnotation1Bindings().list().size());
        assertTrue(service1.getService0Bindings().isBounded());
        assertEquals(4, service1.getService0Bindings().list().size());
        assertFalse(service1.getStringBindings().isBounded());
        assertEquals(0, service1.getStringBindings().list().size());
    }
}