package abzu.container.classic;

import abzu.container.api.AnnotatedBindings;
import abzu.container.api.TypedBindings;

public class Service1Impl0 implements Service1 {
    private final Service0 service0;
    private final AnnotatedBindings<Annotation1> annotation1Bindings;
    private final TypedBindings<Service0> service0Bindings;
    private final TypedBindings<String> stringBindings;

    public Service1Impl0(Service0 service0, AnnotatedBindings<Annotation1> annotation1Bindings, TypedBindings<Service0> service0Bindings, TypedBindings<String> stringBindings) {
        this.service0 = service0;
        this.annotation1Bindings = annotation1Bindings;
        this.service0Bindings = service0Bindings;
        this.stringBindings = stringBindings;
    }

    @Override
    public Service0 getService0() {
        return service0;
    }

    @Override
    public AnnotatedBindings<Annotation1> getAnnotation1Bindings() {
        return annotation1Bindings;
    }

    @Override
    public TypedBindings<Service0> getService0Bindings() {
        return service0Bindings;
    }

    @Override
    public TypedBindings<String> getStringBindings() {
        return stringBindings;
    }
}
