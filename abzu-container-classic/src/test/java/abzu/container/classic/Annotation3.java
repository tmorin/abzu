package abzu.container.classic;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Annotation3 {
    Annotation3 INSTANCE = new Annotation3() {
        @Override
        public Class<? extends Annotation> annotationType() {
            return Annotation3.class;
        }
    };
}
