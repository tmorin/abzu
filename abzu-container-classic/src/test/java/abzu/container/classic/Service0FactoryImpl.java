package abzu.container.classic;

public class Service0FactoryImpl implements Service0Factory {
    @Override
    public Service0 get() {
        return new Service0Impl0();
    }
}
