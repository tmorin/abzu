/**
 * A classical implementation of the container library of abzu.
 */
module abzu.container.classic {

    requires org.slf4j;

    requires abzu.container.api;

    provides abzu.container.api.BinderProvider with abzu.container.classic.ClassicBinderProvider;
    provides abzu.container.api.ContainerProvider with abzu.container.classic.ClassicContainerProvider;

}