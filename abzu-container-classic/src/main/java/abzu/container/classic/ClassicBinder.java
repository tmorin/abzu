package abzu.container.classic;

import abzu.container.api.Binder;
import abzu.container.api.ConfigurableBinding;
import abzu.container.api.ConfiguredBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

class ClassicBinder implements Binder {

    private final static Logger log = LoggerFactory.getLogger(ClassicBinder.class);

    private final Map<Class<?>, List<ConfiguredBinding<?>>> bindings = new HashMap<>();

    @Override
    public Map<Class<?>, List<ConfiguredBinding<?>>> getConfiguredBindings() {
        return Collections.unmodifiableMap(bindings);
    }

    private List<ConfiguredBinding<?>> resolveList(final Class<?> contract) {
        if (!bindings.containsKey(Objects.requireNonNull(contract))) {
            bindings.put(contract, new ArrayList<>());
        }
        return bindings.get(contract);
    }

    @Override
    public <C> ConfigurableBinding<C> bind(final Class<C> type) {
        final var binding = new ClassicBinding<>(Objects.requireNonNull(type));
        log.debug("bind [{}]", type);
        resolveList(type).add(binding);
        return binding;
    }

}
