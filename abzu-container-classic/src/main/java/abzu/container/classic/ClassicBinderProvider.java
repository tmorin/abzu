package abzu.container.classic;

import abzu.container.api.Binder;
import abzu.container.api.BinderProvider;

public class ClassicBinderProvider implements BinderProvider {
    @Override
    public Binder provide() {
        return new ClassicBinder();
    }
}
