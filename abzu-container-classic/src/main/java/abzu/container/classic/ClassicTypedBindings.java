package abzu.container.classic;

import abzu.container.api.ResolvableBinding;
import abzu.container.api.TypedBindings;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

class ClassicTypedBindings<T> implements TypedBindings<T> {

    private final Class<T> type;

    private final List<? extends ResolvableBinding<T>> list;

    ClassicTypedBindings(Class<T> type, List<? extends ResolvableBinding<T>> list) {
        this.type = Objects.requireNonNull(type);
        this.list = Objects.isNull(list) ? Collections.emptyList() : list;
    }

    @Override
    public Class<T> getType() {
        return type;
    }

    @Override
    public List<? extends ResolvableBinding<T>> list() {
        return Collections.unmodifiableList(list);
    }
}
