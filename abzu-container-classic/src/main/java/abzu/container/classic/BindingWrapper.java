package abzu.container.classic;

import abzu.container.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

class BindingWrapper<T> implements ResolvableBinding<T>, Comparable<BindingWrapper<T>> {

    private static final Logger log = LoggerFactory.getLogger(BindingWrapper.class);

    private final Container container;

    private final ClassicBinding<T> binding;

    BindingWrapper(
            final Container container,
            final ConfiguredBinding<T> binding
    ) {
        this.container = requireNonNull(container);

        if (!(binding instanceof ClassicBinding)) {
            throw new IllegalArgumentException("The argument `binding` must be a ClassicBinding instance.");
        }

        this.binding = (ClassicBinding<T>) requireNonNull(binding);
    }

    @SuppressWarnings("unchecked")
    private T createFromImplementation() {
        final Constructor[] constructors = binding.getImplementation().getConstructors();

        if (constructors.length < 1) {
            throw new IllegalStateException(String.format("No constructor found for [%s].", binding.getImplementation()));
        }

        try {
            final Constructor<T> constructor = constructors[0];
            log.debug("Create [{}].", constructor);
            return constructor.newInstance(Arrays.stream(
                    constructor.getGenericParameterTypes())
                    .map(type -> {
                        if (type instanceof ParameterizedType) {
                            // handles TypedBindings and AnnotatedBindings
                            final var pt = (ParameterizedType) type;
                            if (pt.getRawType() instanceof Class) {
                                Class<?> rawType = (Class<?>) pt.getRawType();
                                if (rawType.isAssignableFrom(AnnotatedBindings.class)) {
                                    return container.annotated((Class) pt.getActualTypeArguments()[0]);
                                } else if (rawType.isAssignableFrom(TypedBindings.class)) {
                                    return container.typed((Class) pt.getActualTypeArguments()[0]);
                                } else if (rawType.isAssignableFrom(Optional.class)) {
                                    return container.resolve((Class) pt.getActualTypeArguments()[0]);
                                } else {
                                    return container.get(rawType);
                                }
                            }
                        }

                        if (type instanceof Class) {
                            return container.get((Class) type);
                        }

                        return null;
                    })
                    .toArray()
            );
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalStateException(
                    String.format("Unable to create implementation [%s].", binding.getImplementation()),
                    e
            );
        }
    }

    @Override
    public Optional<T> resolve() {
        if (binding.getValue() != null) {
            return Optional.of(binding.getValue());
        }

        if (binding.getImplementation() != null) {
            log.debug("createInstance from implementation [{}]", binding.getImplementation());
            binding.setValue(createFromImplementation());
            return Optional.of(binding.getValue());
        }

        if (binding.getValueFactoryType() != null && binding.getValueFactory() == null) {
            log.debug("createInstance from factory [{}]", binding.getValueFactoryType());
            binding.setFactory(
                    container.create(
                            binding.getValueFactoryType()
                    )
            );
        }

        if (binding.getValueFactory() != null) {
            log.debug("createInstance from factory [{}]", binding.getValueFactory());
            binding.setValue(
                    binding.getValueFactory().get()
            );
            return Optional.of(binding.getValue());
        }

        return Optional.empty();
    }

    @Override
    public <A extends Annotation> boolean isAnnotated(Class<A> annotationType) {
        return binding.getAnnotations().stream()
                .anyMatch(a -> a.annotationType().equals(annotationType));
    }

    @SuppressWarnings("unchecked")
    @Override
    public <A extends Annotation> Optional<A> getAnnotation(Class<A> annotationType) {
        return binding.getAnnotations().stream()
                .filter(a -> a.annotationType().equals(annotationType))
                .map(a -> (A) a)
                .findFirst();
    }

    @Override
    public Class<T> getType() {
        return binding.getType();
    }

    void dispose() {
        this.binding.dispose();
    }

    @Override
    public int compareTo(BindingWrapper<T> o) {
        return this.binding.getPriority() - o.binding.getPriority();
    }
}
