package abzu.container.classic;

import abzu.container.api.Container;
import abzu.container.api.ContainerProvider;
import abzu.container.api.ContainerProviderParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class ClassicContainerProvider implements ContainerProvider {

    private static final Logger log = LoggerFactory.getLogger(ClassicContainerProvider.class);

    @Override
    public Container provide(ContainerProviderParams params) {
        log.info("Install {}.", Objects.requireNonNull(params).getBinders());

        final var container = new ClassicContainer();

        params.getBinders().forEach(container::install);

        return container;
    }
}
