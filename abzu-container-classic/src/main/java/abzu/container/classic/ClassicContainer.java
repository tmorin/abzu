package abzu.container.classic;

import abzu.container.api.AnnotatedBindings;
import abzu.container.api.Binder;
import abzu.container.api.Container;
import abzu.container.api.TypedBindings;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Collections.singletonList;

class ClassicContainer implements Container {

    private final Map<Class<?>, List<BindingWrapper<?>>> bindingsByType = new ConcurrentHashMap<>();

    private final Map<Class<? extends Annotation>, List<BindingWrapper<?>>> bindingsByAnnotations = new ConcurrentHashMap<>();

    ClassicContainer() {
        bindingsByType.put(
                Container.class,
                new ArrayList<>(singletonList(new BindingWrapper<>(this, new ClassicBinding<>(this))))
        );
    }

    @Override
    @SuppressWarnings("unchecked")
    public void install(final Binder binder) {
        Objects.requireNonNull(binder).getConfiguredBindings().forEach((type, bindings) -> {

            if (!bindingsByType.containsKey(type)) {
                bindingsByType.put(type, new CopyOnWriteArrayList<>());
            }

            bindings.forEach(binding -> {
                // create the wrapper
                final var wrapper = new BindingWrapper<>(this, binding);

                // add the wrapper to the bindingsByType map by its type
                bindingsByType.get(type).add(wrapper);

                binding.getAnnotations().forEach(a -> {
                    if (!bindingsByAnnotations.containsKey(a.annotationType())) {
                        bindingsByAnnotations.put(a.annotationType(), new CopyOnWriteArrayList<>());
                    }
                    // add the wrapper to the bindingsByAnnotations map by its annotation type
                    bindingsByAnnotations.get(a.annotationType()).add(wrapper);
                });
            });

        });
        // sort wrapper according to their priorities
        bindingsByType.values().forEach(l -> Collections.sort((List) l));
        bindingsByAnnotations.values().forEach(l -> Collections.sort((List) l));
    }

    @Override
    public <T> T create(final Class<T> implementation) {
        return new BindingWrapper<>(this, new ClassicBinding<>(Objects.requireNonNull(implementation)))
                .resolve()
                .orElseThrow(
                        () -> new IllegalArgumentException(
                                format("Unable to create [%s].", implementation)
                        )
                );
    }

    @Override
    public boolean has(final Class<?> type) {
        return bindingsByType.containsKey(type);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(final Class<T> type) {
        if (has(type)) {
            return bindingsByType.get(type).stream()
                    .map(BindingWrapper::resolve)
                    .filter(Optional::isPresent)
                    .map(o -> (T) o.get())
                    .findFirst()
                    .orElseThrow(
                            () -> new IllegalArgumentException(
                                    format("The type [%s] is not managed.", type)
                            )
                    );
        }
        throw new IllegalArgumentException(format("The type [%s] is not managed.", type));
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> List<T> list(final Class<T> type) {
        if (has(type)) {
            return bindingsByType.get(type).stream()
                    .map(BindingWrapper::resolve)
                    .filter(Optional::isPresent)
                    .map(o -> (T) o.get())
                    .collect(Collectors.toUnmodifiableList());
        }
        throw new IllegalArgumentException(format("The type [%s] is not managed.", type));
    }

    @Override
    public <A extends Annotation> AnnotatedBindings<A> annotated(final Class<A> annotationType) {
        return new ClassicAnnotatedBindings<>(
                Objects.requireNonNull(annotationType),
                bindingsByAnnotations.get(annotationType)
        );
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> TypedBindings<T> typed(final Class<T> type) {
        return new ClassicTypedBindings(
                Objects.requireNonNull(type),
                bindingsByType.get(type)
        );
    }

    @Override
    public void dispose() {
        for (Map.Entry<Class<?>, List<BindingWrapper<?>>> entry : bindingsByType.entrySet()) {
            for (BindingWrapper<?> wrapper : entry.getValue()) {
                wrapper.dispose();
            }
            entry.getValue().clear();
        }
        bindingsByType.clear();
    }

}
