package abzu.container.classic;

import abzu.container.api.ConfigurableBinding;
import abzu.container.api.ConfiguredBinding;
import abzu.container.api.Factory;

import java.lang.annotation.Annotation;
import java.util.*;

class ClassicBinding<T> implements ConfigurableBinding<T>, ConfiguredBinding<T> {

    private static final int PRIORITY_HIGH = 0;
    private static final int PRIORITY_DEFAULT = Integer.MAX_VALUE / 2;
    private static final int PRIORITY_LOW = Integer.MAX_VALUE;

    private final Class<T> type;

    private Class<? extends T> implementation;

    private T value;

    private Factory<T> factory;

    private Class<? extends Factory<T>> factoryType;

    private int priority = PRIORITY_DEFAULT;

    private Set<Annotation> annotations = new HashSet<>();

    ClassicBinding(Class<T> type) {
        this.type = Objects.requireNonNull(type);
        this.implementation = type;
        this.value = null;
        this.factory = null;
        this.factoryType = null;
        Collections.addAll(annotations, type.getAnnotations());
    }

    @SuppressWarnings("unchecked")
    ClassicBinding(T value) {
        this.value = Objects.requireNonNull(value);
        this.type = (Class<T>) value.getClass();
        this.implementation = null;
        this.factory = null;
        this.factoryType = null;
        Collections.addAll(annotations, type.getAnnotations());
    }

    @Override
    public Class<T> getType() {
        return type;
    }

    @Override
    public <I extends T> ConfigurableBinding<T> to(final Class<I> implementation) {
        this.implementation = Objects.requireNonNull(implementation);
        this.value = null;
        this.factory = null;
        this.factoryType = null;
        return this;
    }

    @Override
    public <I extends T> ConfigurableBinding<T> to(final I value) {
        this.implementation = null;
        this.value = value;
        this.factory = null;
        this.factoryType = null;
        return this;
    }

    @Override
    public ConfigurableBinding<T> toFactory(final Factory<T> factory) {
        this.implementation = null;
        this.value = null;
        this.factory = factory;
        this.factoryType = null;
        return this;
    }

    @Override
    public ConfigurableBinding<T> toFactory(final Class<? extends Factory<T>> factoryType) {
        this.implementation = null;
        this.value = null;
        this.factory = null;
        this.factoryType = Objects.requireNonNull(factoryType);
        return this;
    }

    @Override
    public ConfigurableBinding<T> high() {
        this.priority = PRIORITY_HIGH;
        return this;
    }

    @Override
    public ConfigurableBinding<T> low() {
        this.priority = PRIORITY_LOW;
        return this;
    }

    @Override
    public ConfigurableBinding<T> annotate(final Annotation... annotations) {
        this.annotations.addAll(Arrays.asList(annotations));
        return this;
    }

    @Override
    public Class<? extends T> getImplementation() {
        return this.implementation;
    }

    @Override
    public T getValue() {
        return this.value;
    }

    void setValue(T value) {
        this.value = value;
    }

    @Override
    public Class<? extends Factory<T>> getValueFactoryType() {
        return this.factoryType;
    }

    @Override
    public Factory<T> getValueFactory() {
        return this.factory;
    }

    @Override
    public int getPriority() {
        return this.priority;
    }

    @Override
    public Set<Annotation> getAnnotations() {
        return annotations;
    }

    void dispose() {
        if (Objects.nonNull(factory) && Objects.nonNull(value)) {
            factory.dispose(value);
        }
    }

    void setFactory(Factory<T> factory) {
        this.factory = Objects.requireNonNull(factory);
    }
}
