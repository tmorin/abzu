package abzu.container.classic;

import abzu.container.api.AnnotatedBindings;
import abzu.container.api.ResolvableBinding;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

class ClassicAnnotatedBindings<A extends Annotation> implements AnnotatedBindings<A> {

    private final Class<A> type;

    private final List<? extends ResolvableBinding<?>> list;

    ClassicAnnotatedBindings(
            final Class<A> type,
            final List<? extends ResolvableBinding<?>> list
    ) {
        this.type = Objects.requireNonNull(type);
        this.list = Objects.isNull(list) ? Collections.emptyList() : list;
    }

    @Override
    public Class<A> getType() {
        return type;
    }

    @Override
    public List<? extends ResolvableBinding<?>> list() {
        return Collections.unmodifiableList(list);
    }

}
