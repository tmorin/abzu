# abzu-container-classic

> `abzu-container-classic` is an implementation of `abzu-container-api`.

## Injection point

`abzu-container-classic` supports only the injection by constructors.
If the class to create has several constructors, the first one will be used.

```java
package abzu.container.api;

interface Service0 {
}

interface Service1 {
}

class Service1Impl {
    private final Service0 service0;
    private final Optional<Service1> oService1;
    
    Service1Impl constructor(
            Service0 service0,
            Optional<Service1> oService1
    ) {
        this.service0 = service0;
        this.oService1 = oService1;
    }
}
```
