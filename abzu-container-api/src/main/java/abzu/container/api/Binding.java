package abzu.container.api;

/**
 * A binding.
 *
 * @param <T> the type of the binding.
 */
public interface Binding<T> {

    /**
     * @return the type
     */
    Class<T> getType();

}
