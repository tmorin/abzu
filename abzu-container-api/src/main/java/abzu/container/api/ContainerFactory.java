package abzu.container.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

/**
 * A factory to create a {@link Container} from a {@link ContainerProvider} loaded using {@link ServiceLoader}.
 * <pre>{@code
 * Container container = ContainerFactory.builder().build().create();
 * }</pre>
 */
public class ContainerFactory {

    private static final Logger log = LoggerFactory.getLogger(ContainerFactory.class);
    /**
     * The parameters given to {@link ContainerProvider}
     */
    private final ContainerProviderParams params;

    private ContainerFactory(final ContainerProviderParams params) {
        this.params = params;
    }

    /**
     * @return get a fresh builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Create a fresh {@link Container}
     *
     * @return the container
     */
    public Container create() {
        final var providers = ServiceLoader.load(ContainerProvider.class);

        final var iterator = providers.iterator();

        if (!iterator.hasNext()) {
            throw new IllegalStateException("No ContainerProvider defined!");
        }

        final var provider = iterator.next();

        if (iterator.hasNext()) {
            log.warn("More than one ContainerProvider are defined!");
        }

        return provider.provide(params);
    }

    /**
     * Builder to create the factory.
     */
    public static class Builder {

        private List<Binder> binders = new ArrayList<>();

        private boolean discovery = true;

        /**
         * Add a binder.
         *
         * @param binder the binder
         * @return the builder
         */
        public Builder binder(final Binder binder) {
            binders.add(binder);
            return this;
        }

        /**
         * Skip the discovery of binders from {@link ServiceLoader}.
         *
         * @return the builder
         */
        public Builder skipDiscovery() {
            this.discovery = false;
            return this;
        }

        /**
         * Create the factory.
         *
         * @return the factory
         */
        public ContainerFactory build() {
            if (discovery) {
                binders.addAll(
                        ServiceLoader.load(Binder.class)
                                .stream()
                                .map(ServiceLoader.Provider::get)
                                .collect(Collectors.toList())
                );
            }

            return new ContainerFactory(new ContainerProviderParams(
                    binders
            ));
        }
    }

}
