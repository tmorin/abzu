package abzu.container.api;

/**
 * Factory to create the value of a binding.
 *
 * @param <T> the type of the binding
 */
public interface Factory<T> {

    /**
     * @return the value of the binding
     */
    T get();

    /**
     * Hook to dispose the value of a binding when the container shut down.
     *
     * @param value the value to dispose
     */
    default void dispose(T value) {
    }

}
