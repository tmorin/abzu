package abzu.container.api;

/**
 * Provide a fresh {@link Binder}.
 */
public interface BinderProvider {

    /**
     * Create a binder.
     *
     * @return the binder
     */
    Binder provide();

}
