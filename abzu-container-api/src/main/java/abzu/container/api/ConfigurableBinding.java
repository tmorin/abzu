package abzu.container.api;

import java.lang.annotation.Annotation;

/**
 * A builder to configure a contract.
 *
 * @param <T> the type of the binding
 */
public interface ConfigurableBinding<T> {

    /**
     * Binds the type to an implementation class.
     *
     * @param implementation the class
     * @param <I>            the type of the implementation
     * @return the current builder
     */
    <I extends T> ConfigurableBinding<T> to(Class<I> implementation);

    /**
     * Binds the type to an existing instance.
     *
     * @param value the instance
     * @param <I>   the type of the instance
     * @return the builder
     */
    <I extends T> ConfigurableBinding<T> to(I value);

    /**
     * Binds the type to a factory type.
     *
     * @param factory the supplier
     * @return the builder
     */
    ConfigurableBinding<T> toFactory(Factory<T> factory);

    /**
     * Binds the type to a supplier.
     *
     * @param factoryType the supplier type
     * @return the builder
     */
    ConfigurableBinding<T> toFactory(Class<? extends Factory<T>> factoryType);

    /**
     * Set the priority to high.
     * This value is used to sort bindings and instance using the services of {@link abzu.container.api.Container}.
     *
     * @return the builder
     */
    ConfigurableBinding<T> high();

    /**
     * Set the priority to low.
     * This value is used to sort bindings and instance using the services of {@link abzu.container.api.Container}.
     *
     * @return the builder
     */
    ConfigurableBinding<T> low();

    /**
     * Attaches annotations to the binding.
     *
     * @param annotations the annotations
     * @return the builder
     */
    ConfigurableBinding<T> annotate(Annotation... annotations);
}
