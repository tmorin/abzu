package abzu.container.api;

import java.lang.annotation.Annotation;
import java.util.Set;

/**
 * A binding ready to be installed in a {@link abzu.container.api.Container}.
 */
public interface ConfiguredBinding<T> extends Binding<T> {

    /**
     * @return the implementation
     */
    Class<? extends T> getImplementation();

    /**
     * @return the value
     */
    T getValue();

    /**
     * @return the supplier type
     */
    Class<? extends Factory<T>> getValueFactoryType();

    /**
     * @return the supplier
     */
    Factory<T> getValueFactory();

    /**
     * @return the priority
     */
    int getPriority();

    /**
     * @return the attached annotations
     */
    Set<Annotation> getAnnotations();

}
