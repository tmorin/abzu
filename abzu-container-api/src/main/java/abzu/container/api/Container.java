package abzu.container.api;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Optional;

/**
 * A {@link Container} own a set of bindings and manage the life cycle of the underlying instances.
 */
public interface Container {

    /**
     * To add bindings to the container.
     *
     * @param binder the binder
     */
    void install(Binder binder);

    /**
     * Creates an instance from an implementation class which will not be managed by the container.
     *
     * @param implementation the implementation class
     * @param <T>            the type of the contract
     * @return the created instance
     */
    <T> T create(Class<T> implementation);

    /**
     * Check if the container managed a type.
     *
     * @param type the type
     * @return true if the type is managed
     */
    boolean has(Class<?> type);

    /**
     * Get a value from a type.
     *
     * @param type the type
     * @param <T>  the type
     * @return the value
     */
    <T> T get(Class<T> type);

    /**
     * Resolve an optional value from a type.
     *
     * @param type the type
     * @param <T>  the type
     * @return the optional value
     */
    default <T> Optional<T> resolve(final Class<T> type) {
        return has(type)
                ? Optional.of(get(type))
                : Optional.empty();
    }

    /**
     * Get values from a type.
     *
     * @param type the type
     * @param <T>  the type
     * @return the value
     */
    <T> List<T> list(Class<T> type);

    /**
     * Get an helper about bindings annotated by a given annotation type.
     *
     * @param annotationType the annotation type
     * @param <A>            the annotation type
     * @return the helper
     */
    <A extends Annotation> AnnotatedBindings<A> annotated(Class<A> annotationType);

    /**
     * Get an helper about bindings typed by a given type.
     *
     * @param type the type
     * @param <T>  the type
     * @return the helper
     */
    <T> TypedBindings<T> typed(Class<T> type);

    /**
     * Dispose the container.
     */
    void dispose();

}
