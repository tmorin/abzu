package abzu.container.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * This abstract class is a wrapper of a {@link Binder}.
 * The wrapped {@link Binder} is created by the service load {@link BinderProvider}.
 */
public abstract class AbstractBinder implements Binder {

    private final static Logger log = LoggerFactory.getLogger(AbstractBinder.class);

    private final Binder binder;

    private boolean configured = false;

    public AbstractBinder() {
        binder = createBinder();
    }

    private Binder createBinder() {
        final var providers = ServiceLoader.load(BinderProvider.class);

        final var iterator = providers.iterator();

        if (!iterator.hasNext()) {
            throw new IllegalStateException("No BinderProvider defined!");
        }

        final var provider = iterator.next();

        if (iterator.hasNext()) {
            log.warn("More than one BinderProvider are defined!");
        }

        return provider.provide();
    }

    /**
     * Add there the call to {@link Binder#bind(Class)} in order to bind contracts.
     */
    protected abstract void configure();

    @Override
    public Map<Class<?>, List<ConfiguredBinding<?>>> getConfiguredBindings() {
        if (configured) {
            throw new IllegalStateException("The binder has already been configured!");
        }
        configured = true;
        configure();
        return binder.getConfiguredBindings();
    }

    @Override
    public <T> ConfigurableBinding<T> bind(final Class<T> type) {
        return binder.bind(type);
    }

}
