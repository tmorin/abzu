package abzu.container.api;

import java.util.List;

/**
 * The parameters of the {@link Container} to provide.
 */
public class ContainerProviderParams {

    /**
     * The binders
     */
    private final List<Binder> binders;

    ContainerProviderParams(
            final List<Binder> binders
    ) {
        this.binders = binders;
    }

    public List<Binder> getBinders() {
        return binders;
    }

}
