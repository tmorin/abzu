package abzu.container.api;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * Helper to get bindings according to an annotation type.
 *
 * @param <A> the type of the annotation
 */
public interface AnnotatedBindings<A extends Annotation> {

    /**
     * @return The type of the annotation
     */
    Class<A> getType();

    /**
     * @return all annotated bindings
     */
    List<? extends ResolvableBinding<?>> list();

}
