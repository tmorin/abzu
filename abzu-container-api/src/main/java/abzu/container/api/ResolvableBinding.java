package abzu.container.api;

import java.lang.annotation.Annotation;
import java.util.Optional;

/**
 * A binding ready to be resolved.
 *
 * @param <T> the type of the binding
 */
public interface ResolvableBinding<T> extends Binding<T> {

    /**
     * Resolve the instance of the binding.
     *
     * @return the instance
     */
    Optional<T> resolve();

    /**
     * @param annotationType The annotation type
     * @param <A>            the annotation
     * @return true if the binding is annotated with the annotation type
     */
    <A extends Annotation> boolean isAnnotated(Class<A> annotationType);

    /**
     * @param annotationType The annotation type
     * @param <A>            the annotation
     * @return the annotation
     */
    <A extends Annotation> Optional<A> getAnnotation(Class<A> annotationType);

}
