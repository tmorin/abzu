package abzu.container.api;

import java.util.List;
import java.util.Map;

/**
 * A {@link Binder} configures bindings in order to be installed in a {@link Container}.
 */
public interface Binder {

    /**
     * The list of configured bindings by type.
     *
     * @return the bindings
     */
    Map<Class<?>, List<ConfiguredBinding<?>>> getConfiguredBindings();

    /**
     * Create and add a new binding to the binder.
     *
     * @param type the type of the binding
     * @param <T>  the type of the binding
     * @return a configuration builder
     */
    <T> ConfigurableBinding<T> bind(Class<T> type);

}
