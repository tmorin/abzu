package abzu.container.api;

/**
 * Provide a fresh {@link Container}.
 */
public interface ContainerProvider {

    /**
     * Create a container.
     *
     * @param params the parameters
     * @return the container
     */
    Container provide(ContainerProviderParams params);

}
