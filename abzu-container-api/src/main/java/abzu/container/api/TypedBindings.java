package abzu.container.api;

import java.util.List;
import java.util.Optional;

/**
 * Helper to get bindings according to a type.
 *
 * @param <T> the type of the binding
 */
public interface TypedBindings<T> {

    /**
     * @return the type of the bindings
     */
    Class<T> getType();

    /**
     * @return true if the type is bound to something
     */
    default boolean isBounded() {
        return !list().isEmpty();
    }

    /**
     * @return resolve the first instance
     */
    default Optional<T> resolve() {
        return isBounded() ? list().get(0).resolve() : Optional.empty();
    }

    /**
     * @return all bindings
     */
    List<? extends ResolvableBinding<T>> list();

}
