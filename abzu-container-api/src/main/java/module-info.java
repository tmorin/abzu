/**
 * API of the container library of abzu.
 */
module abzu.container.api {

    requires org.slf4j;

    exports abzu.container.api;

    uses abzu.container.api.Binder;
    uses abzu.container.api.BinderProvider;
    uses abzu.container.api.ContainerProvider;

}