package abzu.container.api;

import java.lang.annotation.Annotation;
import java.util.List;

public class DummyContainer implements Container {

    final List<Binder> binders;

    DummyContainer(List<Binder> binders) {
        this.binders = binders;
    }

    @Override
    public void install(Binder binder) {
    }

    @Override
    public <T> T create(Class<T> implementation) {
        return null;
    }

    @Override
    public boolean has(Class<?> type) {
        return false;
    }

    @Override
    public <T> T get(Class<T> type) {
        return null;
    }

    @Override
    public <T> List<T> list(Class<T> type) {
        return null;
    }

    @Override
    public <A extends Annotation> AnnotatedBindings<A> annotated(Class<A> annotationType) {
        return null;
    }

    @Override
    public <T> TypedBindings<T> typed(Class<T> type) {
        return null;
    }

    @Override
    public void dispose() {

    }
}
