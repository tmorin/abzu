package abzu.container.api;

public class DummyBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(String.class);
    }
}
