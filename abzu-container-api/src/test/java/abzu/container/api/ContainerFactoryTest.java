package abzu.container.api;

import org.junit.Test;

import static org.junit.Assert.*;

public class ContainerFactoryTest {

    @Test
    public void shouldCreateContainer() {
        final var container = ContainerFactory.builder()
                .skipDiscovery()
                .build()
                .create();
        assertNotNull(container);
        assertEquals(container.getClass(), DummyContainer.class);
        DummyContainer dummyContainer = (DummyContainer) container;
        assertTrue(dummyContainer.binders.isEmpty());
    }

    @Test
    public void shouldDiscoverBinders() {
        final var container = ContainerFactory.builder()
                .build()
                .create();
        assertNotNull(container);
        assertEquals(container.getClass(), DummyContainer.class);
        DummyContainer dummyContainer = (DummyContainer) container;
        assertEquals(1, dummyContainer.binders.size());
    }
}