package abzu.container.api;

public class DummyContainerProvider implements ContainerProvider {
    @Override
    public Container provide(ContainerProviderParams params) {
        return new DummyContainer(params.getBinders());
    }
}
