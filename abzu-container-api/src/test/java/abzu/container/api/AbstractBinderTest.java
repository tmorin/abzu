package abzu.container.api;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class AbstractBinderTest {

    @Test
    public void shouldConfigure() {
        final var binder = new DummyBinder();
        final var bindings = binder.getConfiguredBindings();
        assertNotNull(bindings);
        verify(DummyBinderProvider.BINDER, times(1)).bind(String.class);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotConfigureTwice() {
        final var binder = new DummyBinder();
        binder.getConfiguredBindings();
        binder.getConfiguredBindings();
    }

}