package abzu.container.api;

import static org.mockito.Mockito.mock;

public class DummyBinderProvider implements BinderProvider {

    static final Binder BINDER = mock(Binder.class);

    @Override
    public Binder provide() {
        return BINDER;
    }
}
