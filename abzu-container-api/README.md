# abzu-container-api

> `abzu-container-api` is the API of the `abzu-container` library.

## Create a container

The interface `Container` is the API of the [IOC](https://en.wikipedia.org/wiki/Inversion_of_control) pattern.

```java
package abzu.container.api;

interface Container {
    
    void install(Binder binder);

    <T> T create(Class<T> implementation);

    boolean has(Class<?> type);

    <T> T get(Class<T> type);

    <T> List<T> list(Class<T> type);

    <A extends Annotation> AnnotatedBindings<A> annotated(Class<A> annotationType);
    
    <T> TypedBindings<T> typed(Class<T> type);

    void dispose();

}
```

The instances of `Container` are created using the factory `ContainerFactory`.
By default, the factory creates a *container* without *bindings*.

```java
package abzu.container.api;

class Example {
    void play() {
        Container container = ContainerFactory.builder().build().create();
    }
}
```

The *bindings* have to be configured implementing the interface `Binder`.

```java
package abzu.container.api;

interface Binder {
    
    Map<Class<?>, List<ConfiguredBinding<?>>> getConfiguredBindings();
    
    <T> ConfigurableBinding<T> bind(Class<T> type);

}
```

`abzu-container-api` provides an abstract class `AbstractBinder` helping to configure *bindings*.

```java
package abzu.container.api;

interface Service0 {
    default String hello() {
        return "hello";
    }
}

class Service0Impl implements Service0 {
}

class Example {
    void play() {
        Container container = ContainerFactory.builder().binder(new AbstractBinder() {
            public void configure() {
                bind(Service0.class).to(Service0Impl.class);
            }
        }).build().create();
    }
}
```

The factory `ContainerFactory` can also be configured to discover *binders* using `ServiceLoader<Binder>`.

*Service0.java*
```java
package abzu.container.api;

class Service0 {
    default String hello() {
        return "hello";
    }
}
```

*Service0Impl.java*
```java
package abzu.container.api;

class Service0Impl implements Service0 {
}
```

*SimpleBinder.java*
```java
package abzu.container.api;

class SimpleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0.class).to(Service0Impl.class);
    }
}
```

*META-INF/services/abzu.container.api.Binder*
```text
abzu.container.api.SimpleBinder
```

*Example.java*
```java
package abzu.container.api;

class Example {
    void play() {
        Container container = ContainerFactory.builder().discoverFromServiceLoader().build().create();
    }
}
```

## Configure bindings

A *binding* is configured calling the method `Binder#bind(Class<T>)` with its type.
The method creates the *binding* and returns an instance of `ConfigurableBinding<T>` to configure it.

By default, the provided *binding type* is also considered as the *implementation class*. 

```java
package abzu.container.api;

class Service0Impl {
    String hello() {
        return "hello";
    }
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0Impl.class);
    }
}
```

When the type is an interface, the *binding* can be configured with an *implementation class*.
The *implementation class* will be created by the *container* when the *value* of the *binding* is required.
Once created, the *value* is cached and the *implementation class* is no longer created.

```java
package abzu.container.api;

interface Service0 {
    default String hello() {
        return "hello";
    }
}

class Service0Impl implements Service0 {
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0.class).to(Service0Impl.class);
    }
}
```

When the instance of the *implementation class* is already available at the *configuration time*,
the *binding* can be configured with it.

```java
package abzu.container.api;

interface Service0 {
    default String hello() {
        return "hello";
    }
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0.class).to(new Service0() {});
    }
}
```

When the creation of the *implementation class* cannot be directly done by the container, a *factory* can be configured.  
The *factory* must implement the interface `abzu.container.api.Factory<T>`.
Where `<T>` is the type of the *binding*.

The *binding* can be configured with a *factory class*.
The *factory class* will be created and called by the *container* when the *value* of the *binding* is required.
Once supplied, the *value* is cached and the *factory* is no longer called.

```java
package abzu.container.api;

interface Service0 {
    default String hello() {
        return "hello";
    }
}

class Service0Impl implements Service0 {
}

class Service0Factory implements Factory<Service0> {
      public Service0 get() {
          return new Service0Impl();
      }
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0.class).toFactory(new Service0Factory());
    }
}
```

When the instance of the *factory class* is already available at the *configuration time*,
the binding can be configured with it.

```java
package abzu.container.api;

interface Service0 {
    default String hello() {
        return "hello";
    }
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0.class).toFactory(() -> new Service0() {});
    }
}
```

Some use cases require to group *bindings* which are not sharing the same type.
In this case, the *annotations* can be used to mark *bindings*.
Those *annotations* can then be used to filter *bindings*.

Annotations can be attached to a *binding* from its type or from its *configuration*.

```java
package abzu.container.api;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@interface Annotation0 {
    Annotation0 INSTANCE = new Annotation0() {
        @Override
        public Class<? extends Annotation> annotationType() {
            return Annotation0.class;
        }
    };
}

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@interface Annotation1 {
}

class Service0Impl0 {
}

@Annotation1
class Service0Impl1 {
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0Impl0.class).annotate(Annotation0.INSTANCE);
        bind(Service0Impl1.class);
    }
}
```

## Get bindings and underlying values

The interface `Container` provides methods to resolve instances.

The following snippet gets the first instance and then the list of instances related to the *binding type* `Service0`.

```java
package abzu.container.api;

interface Service0 {
    default String hello() {
        return "hello";
    }
}

class Service0Impl0 implements Service0 {
}

class Service0Impl1 implements Service0 {
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0.class).to(Service0Impl0.class);
        bind(Service0.class).to(Service0Impl1.class);
    }
}

class Example {
    void play() {
        Container container = ContainerFactory.builder().binder(new ExampleBinder()).build().create();
        // check the type is managed
        assert container.has(Service0.class);
        // get the first resolved instance
        Service0 firstService0 = container.get(Service0.class);
        assert service0Impl0.getClass().equals(Service0Impl0.class);
        // get all resolved instances
        List<Service0> service0List = container.list(Service0.class);
        assert service0List.size() == 2;
    }
}
```

### Typed bindings

The first way is to resolve all *bindings* sharing the same *binding type*.
This resolution is made by the method `Container#typed(Class<T>)`
which returns an instance of `TypedBindings<T>`.

```java
package abzu.container.api;

interface TypedBindings<T> {

    Class<T> getType();

    List<? extends ResolvableBinding<T>> list();

}
```

The following snippet get the *bindings* related to the type `Service0`.

```java
package abzu.container.api;

interface Service0 {
    default String hello() {
        return "hello";
    }
}

class Service0Impl0 implements Service0 {
}

class Service0Impl1 implements Service0 {
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0.class).to(Service0Impl0.class);
        bind(Service0.class).to(Service0Impl1.class);
    }
}

class Example {
    void play() {
        Container container = ContainerFactory.builder().binder(new ExampleBinder()).build().create();
        TypedBindings<Service0> service0Bindings = container.typed(Service0.class);
        assert service0Bindings.getType().equals(Service0.class);
        assert service0Bindings.list().size() == 2;
    }
}
```

Instances of `TypedBindings<T>` can also be injected.

### Annotated bindings

The second way is to get all bindings annotated by the same annotation type.
This resolution is made by the method `Container#annotated(Class<A>)`
which returns an instance of `AnnotatedBindings<A extends Annotation>`.

```java
package abzu.container.api;

interface AnnotatedBindings<A extends Annotation> {

    Class<A> getType();

    List<? extends ResolvableBinding<?>> list();

}
```

The following snippet get the *bindings* related to the annotation types `Annotation0` and `Annotation1`.

```java
package abzu.container.api;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@interface Annotation0 {
    Annotation0 INSTANCE = new Annotation0() { public Class<? extends Annotation> annotationType() { return Annotation0.class; } };
}

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@interface Annotation1 {
}

class Service0Impl0 {
}

@Annotation1
class Service0Impl1 {
}

class ExampleBinder extends AbstractBinder {
    public void configure() {
        bind(Service0Impl0.class).annotate(Annotation0.INSTANCE);
        bind(Service0Impl1.class).annotate(Annotation0.INSTANCE);
    }
}

class Example {
    void play() {
        Container container = ContainerFactory.builder(new ExampleBinder()).build().create();
        
        AnnotatedBindings<Annotation0> annotation0Bindings = container.getAnnotatedBindings(Annotation0.class);
        assert annotation0Bindings.getType().equals(Annotation0.class);
        assert annotation0Bindings.list().size() == 2;
        
        AnnotatedBindings<Annotation1> annotation1Bindings = container.getAnnotatedBindings(Annotation1.class);
        assert annotation1Bindings.getType().equals(Annotation1.class);
        assert annotation1Bindings.list().size() == 1;
    }
}
```

Instances of `AnnotatedBindings<A extends Annotation>` can also be injected.

## Create values

Values can be created from an *implementation class* even if no related *bindings* have been configured.

```java
package abzu.container.api;

class Service0Impl {
}

class Example {
    void play() {
        Container container = ContainerFactory.builder().build().create();
        Service0Impl service0Impl = container.create(Service0Impl.class);
        assert service0Impl != null;
    }
}
```
