package abzu.config.api;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * The parameters of the {@link Config} to provide.
 */
public class ConfigProviderParams {

    /**
     * The URI list of properties files.
     */
    private final List<URI> uris;

    /**
     * When true, entries are looking for in the System Property {{@link System#getProperty(String)}}
     */
    private final boolean activateSystemProperty;

    /**
     * The prefix used to discover System Properties
     */
    private final String systemPropertyPrefix;

    public ConfigProviderParams(List<URI> uris, boolean activateSystemProperty, String systemPropertyPrefix) {
        this.uris = uris;
        this.activateSystemProperty = activateSystemProperty;
        this.systemPropertyPrefix = systemPropertyPrefix;
    }

    public List<URI> getUris() {
        return uris;
    }

    public boolean isActivateSystemProperty() {
        return activateSystemProperty;
    }

    public String getSystemPropertyPrefix() {
        return systemPropertyPrefix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConfigProviderParams)) return false;
        ConfigProviderParams that = (ConfigProviderParams) o;
        return Objects.equals(uris, that.uris);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uris);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ConfigProviderParams.class.getSimpleName() + "[", "]")
                .toString();
    }
}
