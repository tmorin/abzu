package abzu.config.api;

/**
 * Provide a fresh {@link Config}.
 */
public interface ConfigProvider {

    /**
     * Create a config.
     *
     * @param params the parameters
     * @return the config
     */
    Config provide(final ConfigProviderParams params);

}
