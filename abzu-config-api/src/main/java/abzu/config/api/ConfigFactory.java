package abzu.config.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ServiceLoader;

/**
 * A factory to creating a {@link Config} from a {@link ConfigProvider} loaded using {@link ServiceLoader}.
 */
public class ConfigFactory {

    private static final Logger log = LoggerFactory.getLogger(ConfigFactory.class);

    private final List<URI> uris;

    private final boolean activateSystemProperty;

    private final String systemPropertyPrefix;

    public ConfigFactory(List<URI> uris, boolean activateSystemProperty, String systemPropertyPrefix) {
        this.uris = uris;
        this.activateSystemProperty = activateSystemProperty;
        this.systemPropertyPrefix = systemPropertyPrefix;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Create a fresh {@link Config}
     *
     * @return the binder
     */
    public Config create() {
        final var providers = ServiceLoader.load(ConfigProvider.class);

        final var iterator = providers.iterator();

        if (!iterator.hasNext()) {
            throw new IllegalStateException("No ConfigProvider defined!");
        }

        final var provider = iterator.next();

        if (iterator.hasNext()) {
            log.warn("More than one ConfigProvider are defined!");
        }

        return provider.provide(new ConfigProviderParams(uris, activateSystemProperty, systemPropertyPrefix));
    }

    public static class Builder {

        private final List<URI> uris = new ArrayList<>();

        private boolean activateSystemProperty = true;

        private String systemPropertyPrefix = "config";

        public Builder discoverFromSystemProperty() {
            activateSystemProperty = true;
            systemPropertyPrefix = "config";
            return this;
        }

        public Builder discoverFromSystemProperty(String prefix) {
            activateSystemProperty = true;
            systemPropertyPrefix = prefix;
            return this;
        }

        public Builder uri(final URI... uris) {
            for (URI uri : uris) {
                this.uris.add(Objects.requireNonNull(uri));
            }
            return this;
        }

        public Builder url(final URL... urls) {
            try {
                for (URL url : urls) {
                    this.uris.add(Objects.requireNonNull(url.toURI()));
                }
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
            return this;
        }

        public ConfigFactory build() {
            return new ConfigFactory(uris, activateSystemProperty, systemPropertyPrefix);
        }
    }
}
