package abzu.config.api;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A configuration.
 */
public interface Config {

    /**
     * @return the number of entries
     */
    int size();

    /**
     * @return true if no entries
     */
    boolean isEmpty();

    /**
     * @return the list of keys
     */
    Set<String> keySet();

    /**
     * Convert the config object to a map of string.
     *
     * @return the map
     */
    default Map<String, String> toMap() {
        final Map<String, String> map = new HashMap<>();
        for (String key : keySet()) {
            map.put(key, this.getString(key));
        }
        return Collections.unmodifiableMap(map);
    }

    /**
     * Create another {@link Config} based on a subset of keys starting with the providing prefix.
     *
     * @param prefix the prefix
     * @return the subset of configuration
     */
    Config subset(final String prefix);

    /**
     * Get and convert the string value.
     *
     * @param key the key
     * @param f   the function converting the string value
     * @param <R> the type of the value
     * @return the value
     */
    default <R> R get(final String key, Function<String, R> f) {
        final String value = getString(key);
        return value == null ? null : f.apply(value);
    }

    /**
     * Get the value in String from a key.
     *
     * @param key the key
     * @return the value
     */
    String getString(final String key);

    /**
     * Get the value in Integer from a key.
     *
     * @param key the ley
     * @return the value
     */
    default Integer getInteger(final String key) {
        return get(key, Integer::parseInt);
    }

    /**
     * Get the value in Double from a key.
     *
     * @param key the key
     * @return the value
     */
    default Double getDouble(final String key) {
        return get(key, Double::parseDouble);
    }

    /**
     * Get the value in Long from a key.
     *
     * @param key the key
     * @return the value
     */
    default Long getLong(final String key) {
        return get(key, Long::parseLong);
    }

    /**
     * Get the value in Boolean from a key.
     *
     * @param key the key
     * @return the value
     */
    default Boolean getBoolean(final String key) {
        return get(key, Boolean::parseBoolean);
    }

    /**
     * Get a list of Strings from a key.
     *
     * @param key the key
     * @return the list
     */
    List<String> getListOfString(final String key);

    /**
     * Get a list of converted values from a key
     *
     * @param key the key
     * @param f   the function converting the string value
     * @param <I> the type of value
     * @return the list
     */
    default <I> List<I> getList(final String key, Function<String, I> f) {
        return getListOfString(key).stream().map(f).collect(Collectors.toUnmodifiableList());
    }

    /**
     * Get a list of Integers from a key.
     *
     * @param key the key
     * @return the list
     */
    default List<Integer> getListOfInteger(final String key) {
        return getList(key, Integer::parseInt);
    }

    /**
     * Get a list of Doubles from a key.
     *
     * @param key the key
     * @return the list
     */
    default List<Double> getListOfDouble(final String key) {
        return getList(key, Double::parseDouble);
    }

    /**
     * Get a list of Longs from a key.
     *
     * @param key the key
     * @return the list
     */
    default List<Long> getListOfLong(final String key) {
        return getList(key, Long::parseLong);
    }

    /**
     * Get a list of Booleans from a key.
     *
     * @param key the key
     * @return the list
     */
    default List<Boolean> getListOfBoolean(final String key) {
        return getList(key, Boolean::parseBoolean);
    }

}
