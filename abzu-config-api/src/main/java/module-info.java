/**
 * API of the config library of abzu.
 */
module abzu.config.api {

    requires org.slf4j;

    exports abzu.config.api;

    uses abzu.config.api.ConfigProvider;

}