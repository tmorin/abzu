package abzu.config.api;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class DummyConfig implements Config {
    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Set<String> keySet() {
        return Collections.emptySet();
    }

    @Override
    public Config subset(String prefix) {
        return this;
    }

    @Override
    public String getString(String key) {
        return null;
    }

    @Override
    public List<String> getListOfString(String key) {
        return Collections.emptyList();
    }
}
