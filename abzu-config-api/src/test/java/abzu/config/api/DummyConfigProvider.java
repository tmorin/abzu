package abzu.config.api;

public class DummyConfigProvider implements ConfigProvider {
    @Override
    public Config provide(ConfigProviderParams params) {
        return new DummyConfig();
    }
}
