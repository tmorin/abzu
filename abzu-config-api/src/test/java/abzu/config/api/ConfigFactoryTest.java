package abzu.config.api;

import org.junit.Assert;
import org.junit.Test;

public class ConfigFactoryTest {
    @Test
    public void shouldBeCreated() {
        Config config = ConfigFactory.builder().build().create();
        Assert.assertNotNull(config);
    }
}