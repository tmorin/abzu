# abzu-config-api

> `abzu-config-api` is the API of the `abzu-config` library.

A `Config` is created by the factory `ConfigFactory`.

```java
package abzu.config.api;

class Example {
    void play() {
        Config config = ConfigFactory.builder().build().create();
    }
}
```

The factory can be built providing URL of configurations.

```java
package abzu.config.api;

class Example {
    void play() {
        Config config = ConfigFactory.builder()
            .url(Example.class.getResource("path/to/configuration"))
            .build()
            .create();
    }
}
```
