# abzu

> `abzu` is a set of Java libraries designed to be as light as possible.
Each library is generally delivered with an API package and an implementation package.
That means, if the provided implementation is not good enough it can be easily replaced by a customized one.

The libraries are:
- `abzu-config`: a tiny library to read configurations
  - [abzu-config-api]
  - [abzu-config-classic]
- `abzu-container`: a tiny implementation of [IOC]
  - [abzu-container-api]
  - [abzu-container-classic]
- `abzu-specification`: an implementation of the [Specification Pattern]
  - [abzu-specification]

[abzu-config-api]: abzu-config-api
[abzu-config-classic]: abzu-config-classic
[abzu-container-api]: abzu-container-api
[abzu-container-classic]: abzu-container-classic
[abzu-specification]: abzu-specification
[IOC]: https://en.wikipedia.org/wiki/Inversion_of_control
[Specification Pattern]: https://en.wikipedia.org/wiki/Specification_pattern
