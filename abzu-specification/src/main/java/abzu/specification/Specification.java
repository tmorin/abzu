package abzu.specification;

import java.util.Objects;

/**
 * A specification which checks if a candidate object satisfy it.
 *
 * @param <C> the type of the candidate
 */
public interface Specification<C> {

    /**
     * Check if a candidate satisfy the specification.
     *
     * @param candidate the candidate object
     * @return the result
     */
    boolean isSatisfiedBy(C candidate);

    /**
     * To chain the current specification with another one using the logical operator AND.
     *
     * @param other the other specification
     * @return a new specification
     */
    default Specification<C> and(Specification<C> other) {
        Objects.requireNonNull(other, "the other specification cannot be null");
        return new AndSpecification<>(this, other);
    }

    /**
     * To chain the current specification with another one using the logical operator OR.
     *
     * @param other the other specification
     * @return a new specification
     */
    default Specification<C> or(Specification<C> other) {
        Objects.requireNonNull(other, "the other specification cannot be null");
        return new OrSpecification<>(this, other);
    }

    /**
     * Negate the current specification.
     *
     * @return a new specification
     */
    default Specification<C> not() {
        return new NotSpecification<>(this);
    }

    /**
     * Assemble two specifications with a logical AND.
     *
     * @param spec1 the first specification
     * @param spec2 the second specification
     * @param <C>   the type of the candidate
     * @return a new specification
     */
    static <C> Specification<C> and(Specification<C> spec1, Specification<C> spec2) {
        Objects.requireNonNull(spec1, "first specification cannot be null");
        Objects.requireNonNull(spec2, "second specification cannot be null");
        return spec1.and(spec2);
    }

    /**
     * Assemble two specifications with a logical OR.
     *
     * @param spec1 the first specification
     * @param spec2 the second specification
     * @param <C>   the type of the candidate
     * @return a new specification
     */
    static <C> Specification<C> or(final Specification<C> spec1, final Specification<C> spec2) {
        Objects.requireNonNull(spec1, "first specification cannot be null");
        Objects.requireNonNull(spec2, "second specification cannot be null");
        return spec1.or(spec2);
    }

}
