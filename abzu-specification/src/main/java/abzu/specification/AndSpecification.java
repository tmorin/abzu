package abzu.specification;

import java.util.Objects;

public class AndSpecification<T> implements Specification<T> {

    private Specification<T> spec1;

    private Specification<T> spec2;

    AndSpecification(final Specification<T> spec1, final Specification<T> spec2) {
        this.spec1 = Objects.requireNonNull(spec1, "first specification cannot be null");
        this.spec2 = Objects.requireNonNull(spec2, "second specification cannot be null");
    }

    public boolean isSatisfiedBy(final T candidate) {
        return spec1.isSatisfiedBy(candidate) && spec2.isSatisfiedBy(candidate);
    }

}
