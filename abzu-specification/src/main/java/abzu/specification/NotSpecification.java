package abzu.specification;

import java.util.Objects;

public class NotSpecification<T> implements Specification<T> {

    private Specification<T> spec1;

    NotSpecification(final Specification<T> spec1) {
        this.spec1 = Objects.requireNonNull(spec1, "first specification cannot be null");
    }

    public boolean isSatisfiedBy(final T candidate) {
        return !spec1.isSatisfiedBy(candidate);
    }

}
