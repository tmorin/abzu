/**
 * Implementation of the <a href="https://en.wikipedia.org/wiki/Specification_pattern" target="blank">Specification pattern</a>.
 */
module abzu.specification {
    exports abzu.specification;
}