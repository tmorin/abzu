package abzu.specification;

import java.util.Objects;

class ContainsSpecification implements Specification<String> {

    private final String value;

    ContainsSpecification(final String value) {
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public boolean isSatisfiedBy(String candidate) {
        return candidate.contains(value);
    }
}
