package abzu.specification;

import org.junit.Assert;
import org.junit.Test;

public class SpecificationTest {

    @Test
    public void and() {
        final var spec = Specification.and(new FooSpecification(), new BarSpecification());
        Assert.assertTrue(spec.isSatisfiedBy("foo-bar"));
        Assert.assertFalse(spec.isSatisfiedBy("foo-"));
        Assert.assertFalse(spec.isSatisfiedBy("-bar"));
        Assert.assertFalse(spec.isSatisfiedBy("-"));
    }

    @Test
    public void or() {
        final var spec = Specification.or(new FooSpecification(), new BarSpecification());
        Assert.assertTrue(spec.isSatisfiedBy("foo-bar"));
        Assert.assertTrue(spec.isSatisfiedBy("foo-"));
        Assert.assertTrue(spec.isSatisfiedBy("-bar"));
        Assert.assertFalse(spec.isSatisfiedBy("-"));
    }

    @Test
    public void not() {
        final var fooSpec = new FooSpecification();
        Assert.assertFalse(fooSpec.not().isSatisfiedBy("foo"));
        Assert.assertTrue(fooSpec.not().isSatisfiedBy("bar"));
    }
}