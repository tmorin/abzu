# abzu-specification

> A simple implementation of the [Specification Pattern](https://en.wikipedia.org/wiki/Specification_pattern).

```java
package abzu.specification;

class ContainsSpecification implements Specification<String> {

    private final String value;

    ContainsSpecification(final String value) {
        this.value = Objects.requireNonNull(value);
    }

    @Override
    public boolean isSatisfiedBy(String candidate) {
        return candidate.contains(value);
    }
}

class Example {
    void play() {
        final var fooSpec = new ContainsSpecification("foo");
        final var barSpec = new ContainsSpecification("bar");

        final var fooAndBarSpec = Specification.and(fooSpec, barSpec);
        assert fooAndBarSpec.isSatisfiedBy("foo-bar");
        assert fooAndBarSpec.not().isSatisfiedBy("-");

        final var fooOrBarSpec = Specification.or(fooSpec, barSpec);
        assert fooOrBarSpec.isSatisfiedBy("foo-");
        assert fooOrBarSpec.isSatisfiedBy("-bar");
        assert fooOrBarSpec.not().isSatisfiedBy("-");
    }
}
```
