# abzu-config-classic

> `abzu-config-classic` is an implementation of `abzu-config-api`.

This implementation works only with properties files (i.e. `java.util.Properties`).
The initial properties are the system ones: `java.lang.System#getProperties()`.
When several properties files are provided, the properties of the first ones are overridden by the properties of the latest ones.
