/**
 * A classical implementation of the config library of abzu.
 */
module abzu.config.classic {

    requires abzu.config.api;

    provides abzu.config.api.ConfigProvider with abzu.config.classic.ClassicConfigProvider;

}