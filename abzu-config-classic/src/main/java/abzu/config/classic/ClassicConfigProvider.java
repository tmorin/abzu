package abzu.config.classic;

import abzu.config.api.Config;
import abzu.config.api.ConfigProvider;
import abzu.config.api.ConfigProviderParams;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Properties;

/**
 * Provides the classical implementation.
 */
public class ClassicConfigProvider implements ConfigProvider {

    private static Properties toProperties(URI uri) {
        final var properties = new Properties();
        try (InputStream inputStream = uri.toURL().openStream()) {
            properties.load(inputStream);
            return properties;
        } catch (IOException e) {
            throw new IllegalStateException("unable to load an URI", e);
        }
    }

    @Override
    public Config provide(final ConfigProviderParams params) {
        final var initial = new Properties();

        final var properties = params.getUris().stream()
                .map(ClassicConfigProvider::toProperties)
                .reduce(initial, (properties1, properties2) -> {
                    properties1.putAll(properties2);
                    return properties1;
                });


        final var systemProperties = new Properties();
        systemProperties.putAll(System.getProperties());
        if (params.isActivateSystemProperty()) {
            final var prefix = params.getSystemPropertyPrefix();
            final var prefixKey = prefix.length() > 0 ? prefix + "." : prefix;
            System.getProperties().keySet().stream()
                    .filter(key -> key instanceof String)
                    .map(key -> (String) key)
                    .filter(key -> key.startsWith(prefixKey))
                    .forEach(key -> systemProperties.put(
                            key.substring(prefixKey.length()),
                            System.getProperty(key)
                    ));
        }
        properties.putAll(systemProperties);

        return new ClassicConfig(properties);
    }
}
