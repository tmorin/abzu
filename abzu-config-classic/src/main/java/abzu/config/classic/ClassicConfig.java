package abzu.config.classic;

import abzu.config.api.Config;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The classical implementation.
 */
class ClassicConfig implements Config {

    private static final String LIST_ITEMS_SEPARATOR = ",";

    private final Properties properties;

    ClassicConfig(Properties properties) {
        this.properties = properties;
    }

    @Override
    public int size() {
        return properties.size();
    }

    @Override
    public boolean isEmpty() {
        return properties.isEmpty();
    }

    @Override
    public Set<String> keySet() {
        return properties.keySet()
                .stream().filter(k -> k instanceof String)
                .map(k -> (String) k)
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Config subset(String prefix) {
        final var subProperties = new Properties();
        properties.keySet()
                .stream().filter(k -> k instanceof String)
                .map(k -> (String) k)
                .filter(k -> k.startsWith(prefix))
                .forEach(k -> subProperties.put(k.substring(prefix.length() + 1), properties.get(k)));
        return new ClassicConfig(subProperties);
    }

    @Override
    public String getString(String key) {
        return properties.getProperty(key);
    }

    @Override
    public List<String> getListOfString(String key) {
        final var value = getString(key);
        if (value != null) {
            return Arrays.stream(value.split(LIST_ITEMS_SEPARATOR)).collect(Collectors.toUnmodifiableList());
        }
        return Collections.emptyList();
    }
}
