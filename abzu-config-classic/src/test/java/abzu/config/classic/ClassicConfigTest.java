package abzu.config.classic;

import abzu.config.api.Config;
import abzu.config.api.ConfigFactory;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URISyntaxException;
import java.net.URL;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class ClassicConfigTest {

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("custom.config.foo", "bar");
    }

    @Test
    public void shouldReadProperties() throws URISyntaxException {
        Config config = ConfigFactory
                .builder()
                .uri(ClassicConfigTest.class.getResource("file1.properties").toURI())
                .build()
                .create();

        assertTrue(config.size() > 18);
        assertTrue(config.keySet().size() > 18);
        assertFalse(config.isEmpty());

        assertNotNull(config.getString("java.runtime.version"));

        assertEquals("value1", config.getString("file1.string"));
        assertEquals("doe@git+ssh://foo.bar?k1=v1&k2=v2", config.getString("app.git"));
        assertEquals("jdbc:mysql://localhost:3306/database?useUnicode=true&characterEncoding=utf-8", config.getString("app.db"));
        assertEquals(true, config.getBoolean("file1.boolean_true"));
        assertEquals(false, config.getBoolean("file1.boolean_false"));
        assertEquals(Integer.valueOf(1000), config.getInteger("file1.integer"));
        assertEquals(Double.valueOf(1000.000), config.getDouble("file1.double"));
        assertEquals(Long.valueOf(1000), config.getLong("file1.long"));

        assertEquals(asList("a", "b", "c"), config.getListOfString("file1.list_string"));
        assertEquals(asList(true, false, true), config.getListOfBoolean("file1.list_boolean"));
        assertEquals(asList(1, 2, 3), config.getListOfInteger("file1.list_integer"));
        assertEquals(asList(1.1, 2.2, 3.3), config.getListOfDouble("file1.list_double"));
        assertEquals(asList(1L, 2L, 3L), config.getListOfLong("file1.list_long"));

        assertEquals(config.toMap().get("com.prop1"), "file1_value1");
        assertEquals(config.toMap().get("file1.integer"), "1000");
        assertEquals(config.toMap().get("file1.list_double"), "1.1,2.2,3.3");

        assertNotNull(config.getString("user.dir"));
        assertNotEquals(config.getString("user.dir"), "/somewhere/else");
    }

    @Test
    public void shouldMergeProperties() throws URISyntaxException {
        final URL[] urls = new URL[]{
                ClassicConfigTest.class.getResource("file2.properties"),
                ClassicConfigTest.class.getResource("file3.properties")
        };
        Config config = ConfigFactory
                .builder()
                .uri(ClassicConfigTest.class.getResource("file1.properties").toURI())
                .url(urls)
                .discoverFromSystemProperty("custom.config")
                .build()
                .create();

        assertEquals("file1_value1", config.getString("com.prop1"));
        assertEquals("file2_value2", config.getString("com.prop2"));
        assertEquals("file3_value3", config.getString("com.prop3"));
//        assertEquals("custom.config.foo", config.getString("bar"));
        assertEquals("bar", config.getString("foo"));
        assertEquals("jdoe@git+ssh://foo.bar?k1=v1&k2=v2", config.getString("app.git"));
        assertEquals("jdbc:mysql://127.0.0.1:3306/database?useUnicode=true&characterEncoding=utf-8", config.getString("app.db"));
    }

    @Test
    public void shouldExtractSubset() throws URISyntaxException {
        final URL[] urls = new URL[]{
                ClassicConfigTest.class.getResource("file2.properties"),
                ClassicConfigTest.class.getResource("file3.properties")
        };
        Config config = ConfigFactory
                .builder()
                .uri(ClassicConfigTest.class.getResource("file1.properties").toURI())
                .url(urls)
                .build()
                .create();
        Config subsetAbc = config.subset("a.b.c");
        assertEquals("v1", subsetAbc.getString("n1"));
        assertEquals("v2", subsetAbc.getString("n2"));
        assertEquals("v3", subsetAbc.getString("n3"));
        assertEquals("v4", subsetAbc.getString("n4"));
        Config subsetApp = config.subset("app");
        assertEquals("jdbc:mysql://127.0.0.1:3306/database?useUnicode=true&characterEncoding=utf-8", subsetApp.getString("db"));
    }

}